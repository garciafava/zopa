package ar.com.bgarcia.zopa;

import java.util.List;

import ar.com.bgarcia.zopa.exception.InvalidCsvFormatException;
import ar.com.bgarcia.zopa.exception.InvalidLoanAmountException;
import ar.com.bgarcia.zopa.exception.NoLoanOfferFound;
import ar.com.bgarcia.zopa.model.Lender;
import ar.com.bgarcia.zopa.model.LoanOffer;
import ar.com.bgarcia.zopa.service.LoanOfferService;
import ar.com.bgarcia.zopa.utils.FileUtils;

public class App {

	private static final int MAX_LOAN = 1000;
	private static final int MIN_LOAN = 15000;
	private static final int LOAN_INTERVAL = 100;

	public static void main(String args[]) {
		try {
			final String csvFile = args[0];
			final Integer loan = checkLoanAmount(args[1]);

			List<Lender> lenders = FileUtils.getLenders(csvFile);
			LoanOfferService loanOfferService = new LoanOfferService();
			List<LoanOffer> loanOffers = loanOfferService.findPossibleLoanOffersByLoanAmount(lenders, loan);
			LoanOffer bestOffer = loanOfferService.findBestLoanOffer(loanOffers);
			System.out.println(bestOffer);

		} catch (InvalidLoanAmountException invalidLoanAmount) {
			System.out.println(invalidLoanAmount.getMessage());
		} catch (NoLoanOfferFound noQuoteFound) {
			System.out.println("No Quote found for given csv and loan");
		} catch (InvalidCsvFormatException invalidFormat) {
			System.out.println(invalidFormat.getMessage());
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Invalid Arguments");
		}
	}

	public static Integer checkLoanAmount(final String loanAmount) throws InvalidLoanAmountException {
		Integer loan = null;
		try {
			loan = Integer.parseInt(loanAmount);
		} catch (NumberFormatException ex) {
			throw new InvalidLoanAmountException("The loan amount is invalid");
		}
		if (loan < MAX_LOAN || loan > MIN_LOAN || loan % LOAN_INTERVAL != 0) {
			throw new InvalidLoanAmountException("The loan amount is invalid");
		}
		return loan;
	}
}
