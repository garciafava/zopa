package ar.com.bgarcia.zopa.exception;

@SuppressWarnings("serial")
public class InvalidCsvFormatException extends RuntimeException {

	public InvalidCsvFormatException(String message) {
		super(message);
	}

}
