package ar.com.bgarcia.zopa.exception;

public class InvalidLoanAmountException extends Exception{

	public InvalidLoanAmountException(String message){
		super(message);
	}
}
