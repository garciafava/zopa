package ar.com.bgarcia.zopa.model;

public class Lender {
	private String name;
	private Double rate;
	private Integer available;

	public Lender() {
	}

	public Lender(String name, Double rate, Integer available) {
		this.name = name;
		this.rate = rate;
		this.available = available;
	}

	public String getName() {
		return name;
	}

	public Double getRate() {
		return rate;
	}

	public Integer getAvailable() {
		return available;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((available == null) ? 0 : available.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((rate == null) ? 0 : rate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lender other = (Lender) obj;
		if (available == null) {
			if (other.available != null)
				return false;
		} else if (!available.equals(other.available))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (rate == null) {
			if (other.rate != null)
				return false;
		} else if (!rate.equals(other.rate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Lender [name=" + name + ", rate=" + rate + ", available=" + available + "]";
	}

	public static class Builder {
		private String name;
		private Double rate;
		private Integer available;

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder rate(Double rate) {
			this.rate = rate;
			return this;
		}

		public Builder available(Integer available) {
			this.available = available;
			return this;
		}

		public Lender build() {
			return new Lender(this.name, this.rate, this.available);
		}
	}

}
