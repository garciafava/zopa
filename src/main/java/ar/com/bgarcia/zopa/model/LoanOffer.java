package ar.com.bgarcia.zopa.model;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LoanOffer {
	private Integer loanAmount;
	private List<Lender> lenders = new ArrayList<>();
	private LoanRateDetail loanRateDetail;

	public LoanOffer(Integer loanAmount, List<Lender> lenders, LoanRateDetail loanRateDetail) {
		this.loanAmount = loanAmount;
		this.lenders = lenders;
		this.loanRateDetail = loanRateDetail;
	}

	public List<Lender> getLenders() {
		return lenders;
	}

	public LoanRateDetail getLoanRateDetail() {
		return loanRateDetail;
	}

	@Override
	public String toString() {
		//TODO: DecimalFormat is not threadSafe so check if this might move to another service.
		Locale currentLocale = Locale.getDefault();
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(currentLocale);
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setGroupingSeparator(',');

		final DecimalFormat currencyFormat = new DecimalFormat("£####.##", otherSymbols);
		final DecimalFormat percentageFormat = new DecimalFormat("#.0%", otherSymbols);

		StringBuilder sb = new StringBuilder();
		sb.append("Requested Amount: " + currencyFormat.format(loanAmount));
		sb.append(System.getProperty("line.separator"));
		sb.append("Rate: " + percentageFormat.format(loanRateDetail.getRate()));
		sb.append(System.getProperty("line.separator"));
		sb.append("Monthly Repayment: " + currencyFormat.format(loanRateDetail.getMonthlyRepayment()));
		sb.append(System.getProperty("line.separator"));
		sb.append("Total Repayment: " + currencyFormat.format(loanRateDetail.getTotalRepayment()));

		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lenders == null) ? 0 : lenders.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoanOffer other = (LoanOffer) obj;
		if (lenders == null) {
			if (other.lenders != null)
				return false;
		} else if (!lenders.equals(other.lenders))
			return false;
		return true;
	}

}
