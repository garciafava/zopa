package ar.com.bgarcia.zopa.model;

public class LoanRateDetail {
	public Double rate;
	public Double monthlyRepayment;
	public Double totalRepayment;

	public Double getRate() {
		return rate;
	}

	public Double getMonthlyRepayment() {
		return monthlyRepayment;
	}

	public Double getTotalRepayment() {
		return totalRepayment;
	}

	public LoanRateDetail(Double rate, Double monthlyRepayment, Double totalRepayment) {
		this.rate = rate;
		this.monthlyRepayment = monthlyRepayment;
		this.totalRepayment = totalRepayment;
	}

}
