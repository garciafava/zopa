package ar.com.bgarcia.zopa.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import ar.com.bgarcia.zopa.exception.NoLoanOfferFound;
import ar.com.bgarcia.zopa.model.Lender;
import ar.com.bgarcia.zopa.model.LoanOffer;
import ar.com.bgarcia.zopa.model.LoanRateDetail;

public class LoanOfferService {

	private LoanRateService loanRateService = new LoanRateService();

	public List<LoanOffer> findPossibleLoanOffersByLoanAmount(List<Lender> lenders, int loanAmount) throws NoLoanOfferFound{
		List<LoanOffer> offers = new ArrayList<>();
		findLoanOffers(lenders, loanAmount, new ArrayList<Lender>(), offers);
		return Optional.of(offers)
				.filter(list-> !list.isEmpty())
				.orElseThrow(() -> new NoLoanOfferFound());
	}
	
	public LoanOffer findBestLoanOffer(List<LoanOffer> offers) throws NoLoanOfferFound{
		Comparator<LoanOffer> comparator = (o1, o2) -> Double.compare(o1.getLoanRateDetail().getMonthlyRepayment(), o2.getLoanRateDetail().getMonthlyRepayment());
		
		return offers.stream()
			.min(comparator)
			.orElseThrow(() -> new NoLoanOfferFound());

	}

	public void findLoanOffers(List<Lender> lenders, int loanAmount, List<Lender> partialLenders, List<LoanOffer> offers) {
		int s = 0;
		for (final Lender x : partialLenders)
			s += x.getAvailable();
		if (s == loanAmount) {
			LoanRateDetail rateDetail = loanRateService.getLoanRateDetailForLoanOffer(partialLenders);
			offers.add(new LoanOffer(loanAmount, partialLenders, rateDetail));
		}
		if (s >= loanAmount)
			return ;
		
		for (int i = 0; i < lenders.size(); i++) {
			ArrayList<Lender> remaining = new ArrayList<Lender>();
			Lender n = lenders.get(i);
			for (int j = i + 1; j < lenders.size(); j++)
				remaining.add(lenders.get(j));
			ArrayList<Lender> partial = new ArrayList<Lender>(partialLenders);
			partial.add(n);
			findLoanOffers(remaining, loanAmount, partial, offers);
		}
		return ;
	}
	
	public void setLoanRateService(LoanRateService loanRateService) {
		this.loanRateService = loanRateService;
	}
}
