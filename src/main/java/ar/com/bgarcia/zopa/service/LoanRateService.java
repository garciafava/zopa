package ar.com.bgarcia.zopa.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

import ar.com.bgarcia.zopa.model.Lender;
import ar.com.bgarcia.zopa.model.LoanRateDetail;

public class LoanRateService {

	private static final Integer LOAN_MONTH_LENGTH = 36;

	public LoanRateDetail getLoanRateDetailForLoanOffer(final List<Lender> lenders) {
		Double rate = getLoanRate(lenders);
		Double monthlyRepayment = getMonthlyRepaymentForLenders(lenders);
		Double totalRepayment = getTotalRepayment(monthlyRepayment);

		return new LoanRateDetail(rate, monthlyRepayment, totalRepayment);
	}
	
	public Double getLoanRate(final List<Lender> lenders) {
		return lenders.stream().map(lender -> new BigDecimal(lender.getRate()))
				.reduce(BigDecimal::add)
				.get()
                .divide(BigDecimal.valueOf(lenders.size()), MathContext.DECIMAL64)
                .setScale(2, RoundingMode.HALF_UP)
                .doubleValue();
	}

	public Double getTotalRepayment(final Double monthlyRepayment) {
		return monthlyRepayment * LOAN_MONTH_LENGTH;
	}

	public Double getMonthlyRepaymentForLenders(final List<Lender> lenders) {
		return lenders.stream()
				.mapToDouble(lender -> getMonthlyRepaymentForLender(lender.getAvailable(), new BigDecimal(lender.getRate())).doubleValue())
				.sum();
	}

	public BigDecimal getMonthlyRepaymentForLender(final Integer loanAmount, final BigDecimal rate) {
		return getMonthlyRate(rate).multiply(BigDecimal.valueOf(loanAmount))
				.divide(BigDecimal.valueOf(1)
						.subtract((BigDecimal.valueOf(1)
								.add(getMonthlyRate(rate))
								.pow(-LOAN_MONTH_LENGTH, MathContext.DECIMAL64))), RoundingMode.HALF_UP)
				.setScale(2, RoundingMode.HALF_UP);
	}

	private static BigDecimal getMonthlyRate(final BigDecimal rate) {
		return rate.divide(new BigDecimal(12), 100, RoundingMode.HALF_UP);
	}

}
