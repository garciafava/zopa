package ar.com.bgarcia.zopa.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ar.com.bgarcia.zopa.exception.InvalidCsvFormatException;
import ar.com.bgarcia.zopa.model.Lender;

public class FileUtils {

	public static List<Lender> getLenders(final String fileName) throws InvalidCsvFormatException{
		final List<Lender> lenders = new ArrayList<>();
		
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.filter(line -> !line.startsWith("Lender"))
			.forEach(lender -> {
				List<String> lenderFields = Stream.of(lender.split(","))
						.map(s -> new String(s))
						.collect(Collectors.toList());
				lenders.add(parseLine(lenderFields));
			});
			
		} catch (IOException e) {
			throw new InvalidCsvFormatException("Cannot read file");
		} 

		return lenders;
	}
	
	public static Lender parseLine(List<String> lenderFields) throws InvalidCsvFormatException{
		if(lenderFields.size()!=3)
			throw new InvalidCsvFormatException("Line does not contain 3 fields");
		
		try {
			final String name = lenderFields.get(0);
			final Double rate = Optional.ofNullable(Double.valueOf(lenderFields.get(1)))
					.filter(r -> {return r > 0d;})
					.orElseThrow(() -> new InvalidCsvFormatException("Invalid rate in lender[" + name + "]"));
			final Integer available = Integer.valueOf(lenderFields.get(2));
			
			return new Lender.Builder()
					.name(name)
					.rate(rate)
					.available(available)
					.build();
		}catch (NumberFormatException e) {
			throw new InvalidCsvFormatException("Cannot convert to number");
		}
	}
}
