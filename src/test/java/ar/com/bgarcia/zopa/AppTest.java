package ar.com.bgarcia.zopa;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import ar.com.bgarcia.zopa.exception.InvalidLoanAmountException;

public class AppTest {

	@Test
	public void testCheckLoanAmountOk() throws Exception {
		Integer loanAmount = App.checkLoanAmount("1000");
		assertThat(loanAmount).isEqualTo(1000);
	}

	@Test(expected = InvalidLoanAmountException.class)
	public void testCheckLoanAmountInvalidNumber() throws Exception {
		App.checkLoanAmount("1000d");
	}

	@Test(expected = InvalidLoanAmountException.class)
	public void testCheckLoanAmountInvalidValue() throws Exception {
		App.checkLoanAmount("18000");
	}

}
