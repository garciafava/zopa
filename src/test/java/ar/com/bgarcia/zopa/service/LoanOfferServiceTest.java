package ar.com.bgarcia.zopa.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ar.com.bgarcia.zopa.exception.NoLoanOfferFound;
import ar.com.bgarcia.zopa.model.Lender;
import ar.com.bgarcia.zopa.model.LoanOffer;
import ar.com.bgarcia.zopa.model.LoanRateDetail;

public class LoanOfferServiceTest {
	
	private Lender bob, jane, fred, mary, john, dave, angela;
	private List<Lender> lenders;
	private List<Lender> firstOfferLenders, secondOfferLenders;
	private LoanRateDetail firstLoanRateDetail, secondLoanRateDetail;
	
	private LoanRateService loanRateService = mock(LoanRateService.class);
	private LoanOfferService loanOfferService = new LoanOfferService();

	@Before
	public void setUp() {
		lenders =  new ArrayList<>();
		loanOfferService = new LoanOfferService();
		loanOfferService.setLoanRateService(loanRateService);
		
		lenders.add(bob = new Lender("Bob", 0.075d, 640));
		lenders.add(jane = new Lender("Jane", 0.069d, 480));
		lenders.add(fred = new Lender("Fred", 0.071d, 520));
		lenders.add(mary = new Lender("Mary", 0.104d, 170));
		lenders.add(john = new Lender("John", 0.081d, 320));
		lenders.add(dave = new Lender("Dave", 0.074d, 140));
		lenders.add(angela = new Lender("Angela", 0.071d, 60));
		
		firstOfferLenders = Arrays.asList(jane, fred);
		secondOfferLenders = Arrays.asList(jane, john, dave, angela);
		firstLoanRateDetail = new LoanRateDetail(7.00d, 30.88d, 1111.68d);
		secondLoanRateDetail = new LoanRateDetail(7.00d, 30.88d, 1111.1d);
	}


	@Test
	public void testFindLoanOffersByLoanAmountReturnOffers() throws Exception {
		Integer loanAmount = 1000;

		when(loanRateService.getLoanRateDetailForLoanOffer(firstOfferLenders)).thenReturn(firstLoanRateDetail);
		when(loanRateService.getLoanRateDetailForLoanOffer(secondOfferLenders)).thenReturn(secondLoanRateDetail);
		
		LoanOffer firstQuoteExpected = new LoanOffer(loanAmount, firstOfferLenders, firstLoanRateDetail);
		LoanOffer secondQuoteExpected = new LoanOffer(loanAmount, secondOfferLenders, secondLoanRateDetail);

		List<LoanOffer> quotes = loanOfferService.findPossibleLoanOffersByLoanAmount(lenders, 1000);
		
		assertThat(quotes.size()).isEqualTo(2);
		assertThat(quotes).contains(firstQuoteExpected, secondQuoteExpected);
	}

	@Test
	public void testFindBestLoanOffer() throws Exception {
		Integer loanAmount = 1000;
		final LoanOffer firstQuote = new LoanOffer(loanAmount, firstOfferLenders, firstLoanRateDetail);
		final LoanOffer secondQuote = new LoanOffer(loanAmount, secondOfferLenders, secondLoanRateDetail);

		final LoanOffer bestQuote = loanOfferService.findBestLoanOffer(Arrays.asList(firstQuote, secondQuote));
		assertThat(bestQuote.getLenders()).contains(jane, fred);
		assertThat(bestQuote.getLoanRateDetail().getMonthlyRepayment()).isEqualTo(firstLoanRateDetail.getMonthlyRepayment());
		assertThat(bestQuote.getLoanRateDetail().getTotalRepayment()).isEqualTo(firstLoanRateDetail.getTotalRepayment());
	}

	@Test(expected = NoLoanOfferFound.class)
	public void testFindQuotesByLoanAmountThrowQuoteNotFoundException() throws Exception {
		Integer loanAmount = 18000;
		LoanOfferService service = new LoanOfferService();
		List<Lender> lenders = new ArrayList<>();

		lenders.add(new Lender("belen", 0.08d, 100));
		lenders.add(new Lender("maria", 0.08d, 580));
		lenders.add(new Lender("tomas", 0.08d, 310));
		lenders.add(new Lender("federico", 0.08d, 930));
		lenders.add(new Lender("alejandro", 0.08d, 170));

		service.findPossibleLoanOffersByLoanAmount(lenders, loanAmount);
	}

	@Test(expected = NoLoanOfferFound.class)
	public void testFindBestOfferThrowQuoteNotFoundException() throws Exception {
		loanOfferService.findBestLoanOffer(Arrays.asList());
	}

}
