package ar.com.bgarcia.zopa.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ar.com.bgarcia.zopa.model.Lender;
import ar.com.bgarcia.zopa.model.LoanRateDetail;

public class LoanRateServiceTest {
	private LoanRateService loanRateService;
	private List<Lender> lenders;
	
	@Before
	public void setUp() {
		loanRateService = new LoanRateService();
		lenders =  new ArrayList<>();
		
		lenders.add(new Lender("Jane", 0.069d, 480));
		lenders.add(new Lender("Fred", 0.071d, 520));
	}

	@Test
	public void testGetMonthlyRepaymentForLender() {
		final BigDecimal expectedMonthlyRepayment = new BigDecimal(14.8).setScale(2, RoundingMode.HALF_UP);
		final BigDecimal monthlyPayment = loanRateService.getMonthlyRepaymentForLender(480, new BigDecimal(0.069));
		assertThat(monthlyPayment).isEqualTo(expectedMonthlyRepayment);
	}
	
	@Test
	public void testGetLoanRateReturnRate() {
		final Double expectedLoanRate = 0.07d;
		final Double rate = loanRateService.getLoanRate(lenders);
		assertThat(rate).isEqualTo(expectedLoanRate);
	}
	
	@Test
	public void testGetMonthlyRepaymentForLenderZeroAvailable() {
		final BigDecimal expectedMonthlyRepayment = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);
		final BigDecimal monthlyPayment = loanRateService.getMonthlyRepaymentForLender(0, new BigDecimal(0.069));
		assertThat(monthlyPayment).isEqualTo(expectedMonthlyRepayment);
	}
	
	@Test
	public void testGetLoanRateDetailForLoanOffer() {
		final Double expectedMonthlyRepayment = 30.88d;
		final Double expectedTotalRepayment = 30.88d * 36;
		final LoanRateDetail loanRateDetail = loanRateService.getLoanRateDetailForLoanOffer(lenders);
		assertThat(loanRateDetail.getMonthlyRepayment()).isEqualTo(expectedMonthlyRepayment);
		assertThat(loanRateDetail.getTotalRepayment()).isEqualTo(expectedTotalRepayment);
	}
	
	@Test
	public void testGetMonthlyRepaymentForLenders() {
		final Double expectedMonthlyRepayment = 30.88d;
		final Double monthlyPayment = loanRateService.getMonthlyRepaymentForLenders(lenders);
		assertThat(monthlyPayment).isEqualTo(expectedMonthlyRepayment);
	}
	
	@Test
	public void testGetTotalRepayment() {
		final Double expectedTotalRepayment = 30.88d * 36;
		final Double totalRepayment = loanRateService.getTotalRepayment(30.88d);
		assertThat(totalRepayment).isEqualTo(expectedTotalRepayment);
	}

}
