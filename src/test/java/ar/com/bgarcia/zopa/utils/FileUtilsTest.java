package ar.com.bgarcia.zopa.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import ar.com.bgarcia.zopa.exception.InvalidCsvFormatException;
import ar.com.bgarcia.zopa.model.Lender;

public class FileUtilsTest {

	@Test
	public void testParseLineOk() {
		String name = "Bob";
		Double rate = 0.075d;
		Integer available = 640;

		List<String> lenderStr = Arrays.asList(name, rate.toString(), available.toString());
		Lender lender = FileUtils.parseLine(lenderStr);
		assertThat(lender.getName()).isEqualTo(name);
		assertThat(lender.getRate()).isEqualTo(rate);
		assertThat(lender.getAvailable()).isEqualTo(available);
	}

	@Test(expected = InvalidCsvFormatException.class)
	public void testParseLineInvalidRate() {
		String name = "Bob";
		Integer available = 640;

		List<String> lenderStr = Arrays.asList(name, name, available.toString());
		FileUtils.parseLine(lenderStr);
	}

	@Test(expected = InvalidCsvFormatException.class)
	public void testParseLineInvalidAvailable() {
		String name = "Bob";
		Double rate = 0.075d;
		Double available = 640.0d;

		List<String> lenderStr = Arrays.asList(name, rate.toString(), available.toString());
		FileUtils.parseLine(lenderStr);
	}
	
	@Test(expected = InvalidCsvFormatException.class)
	public void testParseLineInvalidZeroRate() {
		String name = "Bob";
		Double rate = 0d;
		Double available = 640.0d;

		List<String> lenderStr = Arrays.asList(name, rate.toString(), available.toString());
		FileUtils.parseLine(lenderStr);
	}

}
